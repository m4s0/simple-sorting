#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int cmp(const void *x, const void *y) {
    double xx = *(double *) x;
    double yy = *(double *) y;

    if (xx < yy)
        return -1;
    if (xx > yy)
        return 1;

    return 0;
}

int extract_numbers(char *str, double *numbers) {
    int i = 0;
    char *pch = strtok(str, " ");
    while (pch != NULL) {
        numbers[i++] = atof(pch);
        pch = strtok(NULL, "  ");
    }

    return i;
}

int main(int argc, const char *argv[]) {
    FILE *file = fopen(argv[1], "r");
    char line[1024];
    double numbers[1024];

    while (fgets(line, 1024, file)) {
        int n = extract_numbers(line, numbers);
        qsort(numbers, (size_t) n, sizeof(double), cmp);

        for (int i = 0; i < n; i++)
            printf("%0.3f ", numbers[i]);
        printf("\n");
    }
    return 0;
}